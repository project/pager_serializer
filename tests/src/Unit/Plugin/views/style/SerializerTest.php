<?php

namespace Drupal\Tests\pager_serializer\Unit\Plugin\views\style;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Tests\UnitTestCase;
use Drupal\pager_serializer\Plugin\views\style\PagerSerializer;
use Drupal\rest\Plugin\views\display\RestExport;
use Drupal\views\ViewExecutable;
use Prophecy\Argument;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @coversDefaultClass \Drupal\rest\Plugin\views\style\Serializer
 * @group rest
 */
class SerializerTest extends UnitTestCase {

  /**
   * The View instance.
   *
   * @var \Drupal\views\ViewExecutable|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $view;

  /**
   * The RestExport display handler.
   *
   * @var \Drupal\rest\Plugin\views\display\RestExport|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $displayHandler;

  /**
   * ModuleHandler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->view = $this->getMockBuilder(ViewExecutable::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->moduleHandler = $this->getMockBuilder(ModuleHandler::class)
      ->disableOriginalConstructor()
      ->getMock();

    // Make the view result empty so we don't have to mock the row plugin render
    // call.
    $this->view->result = [];

    $this->displayHandler = $this->getMockBuilder(RestExport::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->displayHandler->expects($this->any())
      ->method('getContentType')
      ->willReturn('json');

    $config_map = [
      'pager_serializer.settings' => [
        'rows_label' => 'rows',
        'pager_label' => 'pager',
        'pager_object_enabled' => TRUE,
        'current_page_enabled' => TRUE,
        'current_page_label' => 'current_page',
        'total_items_enabled' => TRUE,
        'total_items_label' => 'total_items',
        'total_pages_enabled' => TRUE,
        'total_pages_label' => 'total_pages',
        'items_per_page_enabled' => TRUE,
        'items_per_page_label' => 'items_per_page',
      ],
    ];

    // Get a stub for the config.factory service.
    $this->configFactory = $this->getConfigFactoryStub($config_map);

    $container = new ContainerBuilder();
    // Set the config.factory in the container also.
    $container->set('config.factory', $this->configFactory);
    \Drupal::setContainer($container);
  }

  /**
   * Tests symfony serializer receives style plugin from the render() method.
   *
   * @covers ::render
   */
  public function testSerializerReceivesOptions() {
    $mock_serializer = $this->prophesize(SerializerInterface::class);

    // This is the main expectation of the test. We want to make sure the
    // serializer options are passed to the SerializerInterface object.
    $mock_serializer->serialize(["rows" => [], "pager" => NULL], 'json', Argument::that(function ($argument) {
      return isset($argument['views_style_plugin']) && $argument['views_style_plugin'] instanceof PagerSerializer;
    }))
      ->willReturn('')
      ->shouldBeCalled();

    $view_serializer_style = new PagerSerializer(
      [],
      'dummy_serializer',
      [],
      $mock_serializer->reveal(),
      ['json', 'xml'],
      ['json' => 'serialization', 'xml' => 'serialization'],
      $this->configFactory,
      $this->moduleHandler);

    $view_serializer_style->options = ['formats' => ['xml', 'json']];
    $view_serializer_style->view = $this->view;
    $view_serializer_style->displayHandler = $this->displayHandler;
    $view_serializer_style->render();
  }

}
