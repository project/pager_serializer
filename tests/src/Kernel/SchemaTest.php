<?php

namespace Drupal\Tests\pager_serializer\Kernel;

use Drupal\Core\Config\Schema\SchemaCheckTrait;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the cluster schema definition.
 *
 * @group pager_serializer
 */
class SchemaTest extends KernelTestBase {

  use SchemaCheckTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'pager_serializer',
  ];

  /**
   * Tests that the cluster schema definition is valid.
   */
  public function testClusterSchema() {
    $config_name = 'pager_serializer.settings';
    $config_data = $this->config($config_name)->get();
    $config_typed = \Drupal::service('config.typed');
    $is_valid = $this->checkConfigSchema($config_typed, $config_name, $config_data);
    $this->assertTrue($is_valid, 'Schema is valid');
  }

}
